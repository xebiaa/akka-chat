package com.xebia.chat

import scala.concurrent.duration._

import akka.actor._
import spray.can.server._
import akka.pattern.ask
import spray.can.websocket
import spray.can.websocket.frame.{ BinaryFrame, TextFrame }
import spray.can.Http
import spray.http.HttpRequest
import spray.can.websocket.FrameCommandFailed
import spray.routing.HttpServiceActor
import spray.json._

import Rooms._

case class Message(user: String, room: String, text: String) extends WsSupport

class WebSocketApi extends Actor with ActorLogging {

  val rooms = context.actorOf(Rooms.props, "the_rooms")

  // rooms ! CreateRoom("Tech_Rally_Chat")
  // rooms ! CreateRoom("Awesome_Chat_Team")
  // rooms ! CreateRoom("Off_topic")

  def receive = {
    // when a new connection comes in we register a WebSocketConnection actor as the per connection handler
    case Http.Connected(remoteAddress, localAddress) ⇒
      val serverConnection = sender()
      val conn = context.actorOf(Props(classOf[WebSocketWorker], serverConnection, rooms))
      serverConnection ! Http.Register(conn)
  }
}

class WebSocketWorker(val serverConnection: ActorRef, rooms: ActorRef) extends HttpServiceActor with websocket.WebSocketServerConnection {
  import WebSocketMessage._
  import context.dispatcher

  context.system.eventStream.subscribe(self, classOf[UserLoggedIn])

  override def receive = handshaking orElse businessLogicNoUpgrade orElse closeLogic

  var userActor: Option[ActorRef] = None

  def businessLogic: Receive = {
    case TextFrame(content) ⇒
      val strContent = content.decodeString("UTF-8")
      log.info(s"Content: $strContent")
      strContent.parseJson.convertTo[WsSupport] match {
        case m: Message  ⇒ userActor.foreach(_ ! Send(m.text, m.room))
        case m: Login    ⇒ rooms ! LoginSession(m.user, self)
        case m: JoinRoom ⇒ rooms ! m
      }

    case x: FrameCommandFailed ⇒
      log.error("frame command failed", x)

    case x: HttpRequest ⇒ // do something

    case m: Message     ⇒ send(TextFrame(m.toJson.prettyPrint))
    case UserLoggedIn(user, username) ⇒
      userActor = Some(user)
      send(TextFrame(LoggedIn(username).toJson.prettyPrint))
      implicit val timeout = akka.util.Timeout(2.seconds)
      rooms.ask(ListRooms).mapTo[Vector[String]].foreach { list ⇒ println("list" + list); send(TextFrame(s""" { "name" : "RoomsList", "payload" : { "rooms" : ${list.toJson.prettyPrint} }} """)) }
  }

  def businessLogicNoUpgrade: Receive = {
    implicit val refFactory: ActorRefFactory = context
    runRoute {
      getFromResourceDirectory("webapp")
    }
  }
}

trait WsSupport extends Product

