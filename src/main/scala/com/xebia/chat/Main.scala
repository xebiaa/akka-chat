package com.xebia.chat

import akka.actor._
import spray.can.Http
import spray.can.server._
import akka.io.IO

object Main extends App {

  implicit val system = ActorSystem()
  import system.dispatcher

  val server = system.actorOf(Props[WebSocketApi], "websocket")

  IO(UHttp) ! Http.Bind(server, "0.0.0.0", 8080)
}
