package com.xebia.chat

import akka.actor._

import Rooms._
import Room._

object User {
  def props(worker: ActorRef, rooms: ActorRef) = Props[User](new User(worker, rooms))
}

class User(worker: ActorRef, rooms: ActorRef) extends Actor {
  def receive = {
    case s: Send ⇒
      println("USER: sending " + s)
      rooms ! s
    case m: RoomMessage ⇒
      println("USER: room message back to websocket: " + m)

      worker ! Message(m.sender.path.name, m.room, m.content)
  }
}
