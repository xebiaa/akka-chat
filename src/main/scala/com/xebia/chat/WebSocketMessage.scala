package com.xebia.chat

import spray.json._
import Rooms._

object WebSocketMessage extends DefaultJsonProtocol {
  import spray.json._

  private val jsonFormatMessageInternal = jsonFormat3(Message.apply)
  private val jsonFormatLoginInternal = jsonFormat1(Login.apply)

  val MessageName = "Message"
  val LoggedInName = "LoggedIn"
  val LoginName = "Login"
  implicit val jsonWriteMessage = jsonWriterAddType(MessageName, jsonFormat3(Message.apply))
  implicit val jsonWriteLoggedIn = jsonWriterAddType(LoggedInName, jsonFormat1(LoggedIn.apply))

  val RoomsListName = "RoomsList"
  implicit val jsonWriteRoomsList = jsonWriterAddType(RoomsListName, jsonFormat1(RoomsList.apply))

  val JoinRoomName = "JoinRoom"

  implicit val WsSupportRead: RootJsonReader[WsSupport] = new RootJsonReader[WsSupport] {
    def read(json: JsValue): WsSupport = json match {
      case JsObject(fields) ⇒ {
        val name = fields("name").convertTo[String]
        name match {
          case MessageName  ⇒ fields("payload").convertTo[Message](jsonFormatMessageInternal)
          case LoginName    ⇒ fields("payload").convertTo[Login](jsonFormatLoginInternal)
          case JoinRoomName ⇒ fields("payload").convertTo[JoinRoom](jsonFormat2(JoinRoom.apply))
        }
      }
      case _ ⇒ deserializationError(s"""JSON object expected.""")
    }
  }

  private def jsonWriterAddType[T](name: String, writer: JsonWriter[T]): JsonWriter[T] = new JsonWriter[T] {
    def write(obj: T) = JsObject("name" -> JsString(name), "payload" -> writer.write(obj))
  }

  case class LoggedIn(user: String)
}
