import com.typesafe.sbt.SbtStartScript
import scalariform.formatter.preferences._

name := "akka-chat"

organization := "xebia"

version := "0.1-SNAPSHOT"

scalaVersion := "2.10.4"

fork := true

scalacOptions := Seq("-encoding", "utf8",
                     "-target:jvm-1.7",
                     "-feature",
                     "-language:implicitConversions",
                     "-language:postfixOps",
                     "-unchecked",
                     "-deprecation",
                     "-Xlog-reflective-calls"
                    )

scalacOptions in Test ++= Seq("-Yrangepos")

mainClass := Some("com.xebia.chat.Main")

resolvers ++= Seq("Sonatype Releases"   at "http://oss.sonatype.org/content/repositories/releases",
                  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
                  "Spray Repository"    at "http://repo.spray.io/",
                  "Spray Nightlies"     at "http://nightlies.spray.io/",
                  "Base64 Repo"         at "http://dl.bintray.com/content/softprops/maven")

libraryDependencies ++= {
  val akkaVersion  = "2.3.3"
  val sprayVersion = "1.3.1"
  Seq(
    "com.typesafe.akka"       %% "akka-actor"                     % akkaVersion,
    "com.typesafe.akka"       %% "akka-persistence-experimental"  % akkaVersion,
    "com.typesafe.akka"       %% "akka-slf4j"                     % akkaVersion,
    "io.spray"                %  "spray-caching"                  % sprayVersion,
    "io.spray"                %  "spray-can"                      % sprayVersion,
    "io.spray"                %  "spray-client"                   % sprayVersion,
    "io.spray"                %  "spray-routing"                  % sprayVersion,
    "io.spray"                %% "spray-json"                     % "1.2.6",
    "com.wandoulabs.akka"     %% "spray-websocket"                % "0.1.1-RC1",
    "com.typesafe.akka"       %% "akka-testkit"                   % akkaVersion    % "test",
    "io.spray"                %  "spray-testkit"                  % sprayVersion   % "test",
    "org.specs2"              %% "specs2-core"                    % "2.3.12"       % "test",
    "org.scalatest"           %  "scalatest_2.10"                 % "2.2.0"        % "test",
    "commons-io"              %  "commons-io"                     % "2.4"          % "test"
  )
}

seq(SbtStartScript.startScriptForClassesSettings: _*)

seq(Revolver.settings: _*)

scalariformSettings

ScalariformKeys.preferences := ScalariformKeys.preferences.value
  .setPreference(AlignSingleLineCaseStatements, true)
  .setPreference(PreserveDanglingCloseParenthesis, true)
  .setPreference(RewriteArrowSymbols, true)
  .setPreference(AlignParameters, false)
  .setPreference(DoubleIndentClassDeclaration, true)
  .setPreference(AlignSingleLineCaseStatements.MaxArrowIndent, 90)
