(function () {
  'use strict';

  angular.module('ChatClient.Login', ['ui.router'])

    .config(function ($stateProvider) {
      $stateProvider.state('login', {
        url: '/',
        controller: 'LoginController',
        templateUrl: 'login/login-form.html'
      });
    })

    .controller('LoginController', function ($scope, $state, WebSocket) {
      $scope.login = function (name) {
        WebSocket.send(JSON.stringify({
          name: 'Login',
          payload: { user: name }
        }));
      };

      $scope.$on('LoggedIn', function (e, data) {
        $scope.setUser(data.user);
        $state.go('rooms');
      });

      $scope.$on('RoomsList', function (e, data) {
        $scope.setRooms(data.rooms);
      });
    });

})();
