(function () {
  'use strict';

  angular.module('ChatClient.Chat', ['ui.router'])

    .config(function ($stateProvider) {
      $stateProvider.state('chat', {
        url: '/chat',
        controller: 'ChatController',
        templateUrl: 'chat/message-list.html'
      });
    })

    .controller('ChatController', function ($scope, WebSocket, Message) {
      $scope.newMessage = {};

      $scope.sendMessage = function (msg) {
        var message = new Message($scope.user, msg.text, $scope.room);
        WebSocket.send(JSON.stringify({
          name: 'Message',
          payload: message.encode()
        }));
        $scope.newMessage = {};
      };
    });

})();
