(function () {
  'use strict';

  angular.module('ChatClient.Rooms', ['ui.router'])

    .config(function ($stateProvider) {
      $stateProvider.state('rooms', {
        url: '/rooms',
        controller: 'RoomsController',
        templateUrl: 'rooms/rooms-list.html'
      });
    })

    .controller('RoomsController', function ($scope, $state, WebSocket) {
      $scope.joinRoom = function (room) {
        WebSocket.send(JSON.stringify({
          name: 'JoinRoom',
          payload: { name: room, user: $scope.user }
        }));
        $state.go('chat');
        $scope.setRoom(room);
      };
    });

})();
