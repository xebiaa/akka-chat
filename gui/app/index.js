(function () {
  'use strict';

  angular.module('ChatClient', [
    'ui.router',
    'angular-websocket',
    'ChatClient.Login',
    'ChatClient.Rooms',
    'ChatClient.Chat'
  ])

    .config(function ($urlRouterProvider, WebSocketProvider) {
      $urlRouterProvider.otherwise('/');

      WebSocketProvider.uri('ws://localhost:8080/');
    })

    .factory('Message', function () {
      var Message = function (user, message, room) {
        this.user = user;
        this.message = message;
        this.room = room;
      };

      Message.decode = function (data) {
        return new Message(
          data.user,
          data.text,
          data.room
        );
      };

      Message.prototype.encode = function () {
        return {
          user: this.user,
          text: this.message,
          room: this.room
        }
      };

      return Message;
    })

    .run(function ($rootScope, WebSocket, Message) {
      $rootScope.user = '';
      $rootScope.room = '';
      $rootScope.rooms = [];
      $rootScope.messages = [];

      $rootScope.setUser = function (name) {
        $rootScope.user = name;
      };

      $rootScope.setRoom = function (name) {
        $rootScope.room = name;
      };

      $rootScope.setRooms = function (rooms) {
        $rootScope.rooms = rooms;
      };

      WebSocket.onmessage(function(event) {
        var data = JSON.parse(event.data);
        if (data.name == 'Message') {
          console.log(data.payload);
          $rootScope.messages.push(Message.decode(data.payload));
        } else {
          $rootScope.$broadcast(data.name, data.payload);
        }
      });
    });

})();
